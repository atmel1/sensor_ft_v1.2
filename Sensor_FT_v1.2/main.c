/*
 * Sensor_FT_v1.1.c
 *
 * Created: 04/04/2018 12:07:09
 * Author : Brandy
 * Reference: 
 *			https://www.easycalculation.com/engineering/electrical/avr-timer-calculator.php
 */ 

#include <avr/io.h>
#include "twi-no_stretch-tiny20-drvr.h"
#define F_CPU 8000000UL
#include <util/delay.h>
#include <avr/interrupt.h>

/* Prototypes here */

void delay100ms(unsigned char);
void init_demo(void);

/* End of prototypes */
/*!  Two variables used to send data to TWI Slave */
unsigned char count1, command; 

/*! 100 msec delay to be tuned by the user, based on choice of AVR and its clock speed */
#define DELAY_VAL_100_MS          0xFFFF-0xe000;  //User: this is an example value when CPU clock speed is 8 MHz.  Adjust this value get
                                                  // your delay value, 100 msec if desired
#define YES                       1
#define NO                        0

/*! Enable/disable LEDs on Slave STK500 or other demo board */
#define LED_DEMO_ON               YES            // PORTA is to display 8 bits of count1 value and PA7 is high while in TWI interrupt servie
#define LED_OUT_CTRL              DDRA           // for defining outputs on LEDs.   May be changed to another port or eliminated.
#define LED_DATA                  PORTA          // For diaplaying count1 value on LEDa

/*! \brief  TWI Interrupt Service Routine
 *
 *Input  : twi_data_from_master();  gets data, sends it to main demo code\n
 *Output : twi_data_to_master(); gets data from main demo code, sends it to master via this ISR\n
 *
 *Asserts ACK, holds clock low until execution of a write to TWI_SLAVE_CTRLB register
 *
****************************************************************************/
//#pragma vector=TWI_SLAVE_vect
ISR(TWI_SLAVE_vect)
{
    uint8_t status = TWI_SLAVE_STATUS & 0xC0;
       
	PORTB = 1;    
        if (status & TWI_DATA_INTERRUPT) {
                if (TWI_SLAVE_STATUS & (1<<TWI_RD_WR_DIR)) {
                        /*Master Reads data from slave*/
                        PORTB |= 4;
                        TWI_SLAVE_DATA = twi_data_to_master(); // This function gets data from the application and loads it into the TWI_SLAVE_DATA (Two Wire Send Data) register             
                  
                        TWI_SLAVE_CTRLB = (uint8_t) ((1<<TWI_SLAVE_CMD1)|(1<<TWI_SLAVE_CMD0)); // release SCL
                } else {
                        /* Master Write */
                        /* Here we read the base address */
                        /* If the base address is with in the range save it, else NACK the Master */
                        TWI_SLAVE_CTRLB = (uint8_t) ((1<<TWI_SLAVE_CMD1)|(1<<TWI_SLAVE_CMD0));
						twi_data_from_master(TWI_SLAVE_DATA) ;  // Callback function: data from master to application                       
                }
        } else if (status & TWI_ADDRESS_STOP_MATCH) {
                /* Address match can happen due to Collision */
                if (TWI_SLAVE_STATUS & TWI_BUS_COLLISION) {
                        /* Clear the Collision Flag */
                        TWI_SLAVE_STATUS = TWI_SLAVE_STATUS;
                        /* Wait for any Start Condition */
                        TWI_SLAVE_CTRLB = (uint8_t) (1<<TWI_SLAVE_CMD1);
                } else {
                        /* Address Match */
                        if (TWI_SLAVE_STATUS & (1<<TWAS)) {
                                /* Execute ACK and then receive next byte or set TWDIF to send the data */                      
                                TWI_SLAVE_CTRLB = (uint8_t)((1<<TWI_SLAVE_CMD1)|(1<<TWI_SLAVE_CMD0));  
                        }  else {
                                /* Stop Condition */
                                /* Wait for any Start Condition */
                                TWI_SLAVE_CTRLB = (uint8_t) (1<<TWI_SLAVE_CMD1);
                        }
                }
        }
}

int Sensor[6] = {0x00, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E};
int Accumu[6] = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00};

ISR(TIM1_OVF_vect){
	Sensor[5] = 0;
	//count1 = 0;
	for (int i = 0; i < 5; i++){
		if (Accumu[i] > 1000){
			Sensor[i] = 1;
		}else {
			Sensor[i] = 0;
		}
		Sensor[5]|= Sensor[i] << i;
		Accumu[i] = 0;
	}
}

ISR(TIM0_COMPA_vect){
	
}

ISR(PCINT0_vect){
	if (PINA & (1 <<PINA0)){
		Accumu[0]++;
	}else if (PINA & (1 <<PINA1)){
		Accumu[1]++;
	}else if (PINA & (1 <<PINA2)){
		Accumu[2]++;
	}else if (PINA & (1 <<PINA3)){
		Accumu[3]++;
	}else if (PINA & (1 <<PINA4)){
		Accumu[4]++;
	}
	count1++;
}

int main(void)
{
	/* initialize Demo */
	init_demo();
	/* initialise TWI Slave */
	twi_slave_initialise();
	CLKMSR |= (0 << CLKMS1) | (0 << CLKMS0);
	/*
	Pg. 20
	CLKPSR: Clock Main Settings Register
	CLKPSR[7:2]: Reserved
	CLKPSR[1:0]: Clock Main Select Bits
	CLKM1 | CLKM0 | Main Clock Source
	  0   |   0   |Calibrated Internal 8 MHz Oscillator <--
	  0   |   1   |Internal 128 kHz Oscillator (WDT Oscillator)
	  1   |   0   |External clock
	  1   |   1   |Reserved
	*/
	DDRB |= (1 << DDB0);
	DDRA |= (0 << DDA4) | (0 << DDA3) | (0 << DDA2) | (0 << DDA1) | (0 << DDA0) ;
	/*
	pg. 43
	The DDxn bit in the DDRx Register selects the direction of this pin. 
	If DDxn is written logic one, Pxn is configured as an output pin. <--
	If DDxn is written logic zero, Pxn is configured as an input pin.
	*/
	sei(); //Global Interrupt Enable
	TCCR1B |= (0 << CS12)|(0 << CS11)|(1 << CS10);
	/*
	pg. 99
	TCCR1B  Timer/Counter1 Control Register B
	ICNC1 | ICES1 |  | WGM13 | WGM12 | CS12 | CS11 | CS10
	ICNC1: Input Capture Noise Canceler
	ICES1: Input Capture Edge Select
	Res: Reserved Bit
	WGM1[3:2]: Waveform Generation Mode
	CS1[2:0]: Clock Select <-- 1M
	CS12|CS11|CS10|Description
	 0  | 0  | 0  |No clock source (Timer/Counter stopped).
	 0  | 0  | 1  |clkI/O/1 (No prescaling) 65.28ms
	 0  | 1  | 0  |clkI/O/8 (From prescaler) 522.24s
	 0  | 1  | 1  |clkI/O/64 (From prescaler) <-- 4.178s
	 1  | 0  | 0  |clkI/O/256 (From prescaler) 16.712s
	 1  | 0  | 1  |clkI/O/1024 (From prescaler) 66.847s
	 1  | 1  | 0  |External clock source on T1 pin. Clock on falling edge.
	 1  | 1  | 1  |External clock source on T1 pin. Clock on rising edge.
	CS1[2:0]: Clock Select <-- 8M
	CS12|CS11|CS10|Description
	 0  | 0  | 0  |No clock source (Timer/Counter stopped).
	 0  | 0  | 1  |clkI/O/1 (No prescaling) 8.19ms
	 0  | 1  | 0  |clkI/O/8 (From prescaler) 65.5ms
	 0  | 1  | 1  |clkI/O/64 (From prescaler) 0.524s
	 1  | 0  | 0  |clkI/O/256 (From prescaler) <-- 2.097s
	 1  | 0  | 1  |clkI/O/1024 (From prescaler) 8.388s
	 1  | 1  | 0  |External clock source on T1 pin. Clock on falling edge.
	 1  | 1  | 1  |External clock source on T1 pin. Clock on rising edge.
	*/
	TIMSK |= (1 << TOIE1);
	/*
	pg. 74
	TIMSK  Timer/Counter Interrupt Mask Register
	ICIE1 |  | OCIE1B | OCIE1A | TOIE1 | OCIE0B | OCIE0A | TOIE0
	OCIE1B: Timer/Counter Output Compare Match B Interrupt Enable
	OCIE1A: Timer/Counter0 Output Compare Match A Interrupt Enable
	TOIE1: Timer/Counter0 Overflow Interrupt Enable  <--
	OCIE0B: Timer/Counter Output Compare Match B Interrupt Enable
	OCIE0A: Timer/Counter0 Output Compare Match A Interrupt Enable
	TOIE0: Timer/Counter0 Overflow Interrupt Enable
	*/
	TWSCRA |= (1 << TWEN) | (1 << TWPME) | (1 << TWSME);
	/*
	pg. 143
	TWSHE |  | TWDIE | TWASIE | TWEN | TWSIE | TWPME | TWSME
	TWSHE: TWI SDA Hold Time Enable
	Res: Reserved Bit
	TWDIE: TWI Data Interrupt Enable
	TWASIE: TWI Address/Stop Interrupt Enable
	TWEN: Two-Wire Interface Enable <--- 
		When this bit is set the slave Two-Wire Interface is enabled.
	TWSIE: TWI Stop Interrupt Enable
	TWPME: TWI Promiscuous Mode Enable <--- 
		When this bit is set the address match logic of the slave TWI responds to all received addresses.
	TWSME: TWI Smart Mode Enable <--- 
		The Acknowledge Action is sent immediately after the TWI data register (TWSD) has been read. 
		Acknowledge Action is defined by the TWAA bit in TWSCRB
	*/
	GIMSK |= (1 << PCIE0);
	/*
	pg. 39
	 |  | PCIE1 | PCIE0 |  |  |  | INT0
	PCIE1: Pin Change Interrupt Enable 1
	PCIE0: Pin Change Interrupt Enable 0 <--
	When the PCIE0 bit is set (one) and the I-bit in the Status Register (SREG) is set (one), pin change interrupt 0 is
	enabled. Any change on any enabled PCINT[7:0] pin will cause an interrupt. The corresponding interrupt of Pin Change
	Interrupt Request is executed from the PCI0 Interrupt Vector. PCINT[7:0] pins are enabled individually by the PCMSK0
	Register.
	INT0: External Interrupt Request 0 Enable
	*/
	PCMSK0 |= (1 << PCINT4) | (1 << PCINT3) | (1 << PCINT2) | (1 << PCINT1) | (1 << PCINT0);
	/*
	pg. 41
	Bits 7:0  PCINT[7:0]: Pin Change Enable Mask 7:0
	Each PCINT[7:0] bit selects whether pin change interrupt is enabled on the corresponding I/O pin. If PCINT[7:0] is set
	and the PCIE0 bit in GIMSK is set, pin change interrupt is enabled on the corresponding I/O pin. If PCINT[7:0] is cleared,
	pin change interrupt on the corresponding I/O pin is disabled.
	*/
	
    while (1) 
    {
		count1++;
		/*
		//Test to calculate F_CPU
		PINB |= (1 << PINB0);
		_delay_ms(200);
		_delay_ms(200);
		_delay_ms(200);
		_delay_ms(200);
		_delay_ms(200);
		*/
		/*
		//Test for Timer Dynamic Change
		if (Counte%6 <= 3){
			TCCR1B = (1 << CS12)|(0 << CS11)|(0 << CS10); //Tarda mas
		}else {
			TCCR1B = (0 << CS12)|(1 << CS11)|(1 << CS10); //Tarda Menos
		}
		*/
		/*
		//Test for ADC Conversion
		PINB |= (1 << PINB0);
		while (ADCSRA & (1 << ADSC) ); // wait till conversion complete
		_delay_ms(200);
		*/
    }
}

/*! /user initialalize function
*
*  Set tinyAVR clock speed from default 1 MHz to 8 Mhz
*  
*  IF LED_DEMO is enabled, set LED port pins to outputs
*/
void init_demo(void){

    /* Clear CLKPSR bits to get AVR to run at 8 MHz  */
  CCP = 0xD8; // wite access signature to Configuration Change Protection Register
  CLKPSR = 0;  // This should change clock from 1 MHz default to 8 MHz
  
#if LED_DEMO_ON
  LED_OUT_CTRL = 0x7f;  //PA7 is SCL on tiny20/tiny40 AVR
#endif
}

/*!  \brief  TWI slave twi_data_to_master driver callback function
 * 
 *   This is an example of a command-specific response logic
 *   to demonstrate how the TWI driver gets data from the application main code
 *   Input: none
 *   Output: for demo purposes, this function returns a value in count1 or ~count1 based on the
 *   value of command, previously sent from the master to the slave and this code
 *   The user may change this function's respoonse to suit a particular application
 *
 */
unsigned char twi_data_to_master(void){
  //unsigned char data_to_master;
  switch(command){
	case 1: return Sensor[0];
		break;
	case 2: return Sensor[1]; 
		break;
	case 3: return Sensor[2]; 
		break;
	case 4: return Sensor[3]; 
		break;
	case 5: return Sensor[4]; 
		break;
	case 6: return Sensor[5];
		break;
	case 7: return count1 % 5; 
		break;
	default: return ~count1;
  }
  //return data_to_master;
}

/*!  \brief  TWI slave twi_data_from_master driver callback function
 *
 *   This is an example of a command-specific response logic
 *   to demonstrate how the TWI driver can send a command from teh TWI master
 *    to the slave code's application
 *   Input:  data from master to slave then to users code
 *   
 *   Output: User code receives new data via the function returning a value
 *  
 */
void twi_data_from_master(unsigned char data){
   command = data;
}